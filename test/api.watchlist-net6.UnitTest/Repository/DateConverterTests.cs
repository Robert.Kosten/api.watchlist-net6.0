using System;
using Amazon.DynamoDBv2.DocumentModel;
using api.watchlist_net6.Repository;
using Xunit;

namespace api.watchlist_net6.UnitTest.Repository;

public class DateConverterTests
{
    [Fact]
    public void FromEntry()
    {
        DateConverter converter = new DateConverter();
        DynamoDBEntry entry = new Primitive("2022-04-19T15:01:22:304Z");
        DateTime dateTime = (DateTime) converter.FromEntry(entry);
        Assert.NotNull(dateTime);
        Assert.Equal(2022, dateTime.Year);
    }
    
    [Fact]
    public void FromEntry_BadDate()
    {
        DateConverter converter = new DateConverter();
        DynamoDBEntry entry = new Primitive("04-19-2022T15:01:22:304Z");
        object dateTime = converter.FromEntry(entry);
        Assert.Null(dateTime);
    }
    
    [Fact]
    public void FromEntry_NoDate()
    {
        DateConverter converter = new DateConverter();
        DynamoDBEntry entry = new Primitive("");
        object dateTime = converter.FromEntry(entry);
        Assert.Null(dateTime);
    }

    [Fact]
    public void ToEntry()
    {
        DateConverter converter = new DateConverter();
        DateTime dateTime = new DateTime(2022, 04, 19, 15, 04, 34);
        string result = converter.ToEntry(dateTime);
        Assert.NotNull(result);
        Assert.True(result.Contains("2022"));
    }
    
    [Fact]
    public void ToEntry_NotADate()
    {
        DateConverter converter = new DateConverter();
        Assert.Throws<ArgumentException>(() => converter.ToEntry(new object()));
    }
    
    [Fact]
    public void ToEntry_NullDate()
    {
        DateConverter converter = new DateConverter();
        DynamoDBNull dbNull = (DynamoDBNull) converter.ToEntry(null);
        Assert.Equal(DynamoDBNull.Null, dbNull);
    }
}