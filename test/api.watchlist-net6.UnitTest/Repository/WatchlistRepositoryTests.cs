using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using api.watchlist_net6.Repository;
using api.watchlist_net6.Repository.Entity;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.Repository;

public class WatchlistRepositoryTests
{
    private readonly Mock<IDynamoDBContext> _mockContext;

    public WatchlistRepositoryTests()
    {
        _mockContext = new Mock<IDynamoDBContext>();
        _mockContext.Setup(x =>
                x.SaveAsync(It.IsAny<Watchlist>(), It.IsAny<DynamoDBOperationConfig>(), It.IsAny<CancellationToken>()))
            .Returns(Task.FromResult(new Watchlist()));
        _mockContext.Setup(x => x.LoadAsync(It.IsAny<Watchlist>(), CancellationToken.None)).ReturnsAsync(new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten"
        });
        AsyncSearch<Watchlist> result = null;
        _mockContext
            .Setup(x => x.FromQueryAsync<Watchlist>(It.IsAny<QueryOperationConfig>(),
                It.IsAny<DynamoDBOperationConfig>())).Returns(result);
    }

    [Fact]
    public async void SaveWatchlistAsync()
    {
        WatchlistRepository watchlistRepository = new WatchlistRepository(_mockContext.Object);
        await watchlistRepository.SaveWatchlist(new Watchlist());
    }
    
    [Fact]
    public async void DeleteWatchlistAsync()
    {
        WatchlistRepository watchlistRepository = new WatchlistRepository(_mockContext.Object);
        await watchlistRepository.DeleteWatchlist(new Watchlist());
    }

    [Fact]
    public async void FindWatchlistAsync()
    {
        WatchlistRepository watchlistRepository = new WatchlistRepository(_mockContext.Object);
        await watchlistRepository.FindWatchlist("rkosten", "NEWWATCHLIST");
    }

    [Fact]
    public async void FindAllWatchlistsByUserAsync()
    {
        WatchlistRepository watchlistRepository = new WatchlistRepository(_mockContext.Object);
        await watchlistRepository.FindAllWatchlistsByUserAsync("rkosten");
    }

}