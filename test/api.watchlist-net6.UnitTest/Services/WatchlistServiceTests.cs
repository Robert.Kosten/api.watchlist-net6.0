using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using api.watchlist_net6.Model;
using api.watchlist_net6.Repository;
using api.watchlist_net6.Repository.Entity;
using api.watchlist_net6.Services;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.Services;

public class WatchlistServiceTests
{
    private readonly Mock<IWatchlistRepository> _watchlistRepository;
    private readonly Mock<ILogger<WatchlistService>> _mockLogger;

    public WatchlistServiceTests()
    {
        _watchlistRepository = new Mock<IWatchlistRepository>();
        _mockLogger = new Mock<ILogger<WatchlistService>>();
    }

    [Fact]
    public async void RetrieveWatchlistNamesAsync()
    {
        List<Watchlist> watchlists = GetWatchlists();
        _watchlistRepository.Setup(x => x.FindAllWatchlistsByUserAsync(It.IsAny<string>()))
            .ReturnsAsync(watchlists);
        IWatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        ICollection<WatchlistDto> watchlistDtos = await watchlistService.RetrieveWatchlistNamesAsync("rkosten");

        IList<WatchlistDto> watchlistDtoList = watchlistDtos.ToList();
        WatchlistDto responseDto = watchlistDtoList[0];
        Assert.Null(responseDto.SviUserId);
        Assert.Equal("NEWWATCHLIST", responseDto.Name);
    }
    
    [Fact]
    public async void RetrieveWatchlistsAsync()
    {
        var watchlists = GetWatchlists();
        _watchlistRepository.Setup(x => x.FindAllWatchlistsByUserAsync(It.IsAny<string>()))
            .ReturnsAsync(watchlists);
        IWatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        ICollection<WatchlistDto> watchlistDtos = await watchlistService.RetrieveWatchlistsAsync("rkosten");

        IList<WatchlistDto> watchlistDtoList = watchlistDtos.ToList();
        WatchlistDto responseDto = watchlistDtoList[0];
        Assert.Equal("rkosten", responseDto.SviUserId);
        Assert.Equal("NEWWATCHLIST", responseDto.Name);
        
        List<WatchlistItemDto> watchlistItemDtos = responseDto.WatchlistItems.ToList();
        WatchlistItemDto watchlistItemDto = watchlistItemDtos[0];
        Assert.Equal("MSFT" ,watchlistItemDto.Symbol);
        Assert.Equal(1, watchlistItemDto.SortOrder);
        Assert.Null(watchlistItemDto.SviUserId);
        Assert.Null(watchlistItemDto.Name);
    }

    [Fact]
    public async void RetrieveWatchlistAsync()
    {
        var watchlists = GetWatchlists();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlists[0]);
        IWatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistDto watchlistDto = await watchlistService.RetrieveWatchlistAsync("rkosten", "NEWWATCHLIST");
        
        Assert.Equal("rkosten", watchlistDto.SviUserId);
        Assert.Equal("NEWWATCHLIST", watchlistDto.Name);
        
        List<WatchlistItemDto> watchlistItemDtos = watchlistDto.WatchlistItems.ToList();
        WatchlistItemDto watchlistItemDto = watchlistItemDtos[0];
        Assert.Equal("MSFT" ,watchlistItemDto.Symbol);
        Assert.Equal(1, watchlistItemDto.SortOrder); ;
        Assert.Null(watchlistItemDto.SviUserId);
        Assert.Null(watchlistItemDto.Name);
    }

    [Fact]
    public async void SaveWatchlistAsync()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);

        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistAsync(watchlistRequestDto);

        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Created, commandResponse.StatusCode);
    }
    
    [Fact]
    public async void SaveWatchlistAsync_Exception_WatchlistExists()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = new Watchlist
        {
             Name = "NEWWATCHLIST",
             SviUserId = "rkosten"
        };
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);

        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistAsync(watchlistRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void SaveWatchlistAsync_Exception_IssueSavingWatchlist()
    {
        var watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);

        _watchlistRepository.Setup(x => x.SaveWatchlist(It.IsAny<Watchlist>())).Throws(new Exception());

        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistAsync(watchlistRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.InternalServerError, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }


    [Fact]
    public async void SaveWatchlistItemAsync()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        Watchlist watchlist = new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten",
            CreatedBy = "user",
            CreatedDate = DateTime.Now,
            LastUpdatedBy = "user",
            LastUpdatedDate = DateTime.Now
        };
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistItemAsync(watchlistItemRequestDto);

        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void SaveWatchlistItemAsync_WatchlistContainsItems()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        Watchlist watchlist = new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten",
            CreatedBy = "user",
            CreatedDate = DateTime.Now,
            LastUpdatedBy = "user",
            LastUpdatedDate = DateTime.Now,
            WatchlistItems = new List<WatchlistItem>
            {
                new WatchlistItem
                {
                    Symbol = "AAPL",
                    SortOrder = 2
                }
            }
        };
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistItemAsync(watchlistItemRequestDto);

        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    
    [Fact]
    public async void SaveWatchlistItemAsync_WatchlistContainsMatchingItem()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 2
        };

        Watchlist watchlist = new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten",
            CreatedBy = "user",
            CreatedDate = DateTime.Now,
            LastUpdatedBy = "user",
            LastUpdatedDate = DateTime.Now,
            WatchlistItems = new List<WatchlistItem>
            {
                new WatchlistItem
                {
                    Symbol = "MSFT",
                    SortOrder = 1
                }
            }
        };
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistItemAsync(watchlistItemRequestDto);

        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void SaveWatchlistItemAsync_Exception_WatchlistDoesNotExist()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistItemAsync(watchlistItemRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void SaveWatchlistItemAsync_Exception_IssueSavingWatchlist()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        var watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);

        _watchlistRepository.Setup(x => x.SaveWatchlist(It.IsAny<Watchlist>())).Throws(new Exception());
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.SaveWatchlistItemAsync(watchlistItemRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.InternalServerError, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }

    [Fact]
    public async void RenameWatchlistAsync()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);

        Watchlist watchlistNew = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.NewName))
            .ReturnsAsync(watchlistNew);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.RenameWatchlistAsync(watchlistRequestDto);

        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void RenameWatchlistAsync_Exception_WatchlistNotFound()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);

        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.RenameWatchlistAsync(watchlistRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void RenameWatchlistAsync_Exception_NewWatchlistNameFound()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);

        Watchlist watchlistNew = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.NewName))
            .ReturnsAsync(watchlistNew);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.RenameWatchlistAsync(watchlistRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void RenameWatchlistAsync_Exception_IssueSavingWatchlist()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);

        Watchlist watchlistNew = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.NewName))
            .ReturnsAsync(watchlistNew);

        _watchlistRepository.Setup(x => x.SaveWatchlist(It.IsAny<Watchlist>())).Throws(new Exception());
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.RenameWatchlistAsync(watchlistRequestDto);

        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.InternalServerError, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }

    [Fact]
    public async void DeleteWatchlistAsync()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistAsync(watchlistRequestDto);
        
        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void DeleteWatchlistAsync_Exception_WatchlistNoFound()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistAsync(watchlistRequestDto);
        
        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void DeleteWatchlistAsync_Exception_IssueDeletingWatchlist()
    {
        WatchlistRequestDto watchlistRequestDto = GetWatchlistRequestDto();

        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name))
            .ReturnsAsync(watchlist);

        _watchlistRepository.Setup(x => x.DeleteWatchlist(It.IsAny<Watchlist>())).Throws(new Exception());
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistAsync(watchlistRequestDto);
        
        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.InternalServerError, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }

    [Fact]
    public async void DeleteWatchlistItemAsync()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        List<WatchlistItem> watchlistItems = new List<WatchlistItem>
        {
            new()
            {
                Symbol = "MSFT",
                SortOrder = 1
            },
            new()
            {
                Symbol = "AAPL",
                SortOrder = 2
            }
        };

        Watchlist watchlist = GetWatchlist();
        watchlist.WatchlistItems = watchlistItems;
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistItemAsync(watchlistItemRequestDto);
        
        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void DeleteWatchlistItemAsync_ItemNotFound()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "ALLY",
            SortOrder = 1
        };

        List<WatchlistItem> watchlistItems = new List<WatchlistItem>
        {
            new()
            {
                Symbol = "MSFT",
                SortOrder = 1
            },
            new()
            {
                Symbol = "AAPL",
                SortOrder = 2
            }
        };

        Watchlist watchlist = GetWatchlist();
        watchlist.WatchlistItems = watchlistItems;
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistItemAsync(watchlistItemRequestDto);
        
        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void DeleteWatchlistItemAsync_NoItemsFound()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "ALLY",
            SortOrder = 1
        };
        
        Watchlist watchlist = GetWatchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistItemAsync(watchlistItemRequestDto);
        
        Assert.True(commandResponse.Success);
        Assert.Equal(HttpStatusCode.Accepted, commandResponse.StatusCode);
        Assert.Null(commandResponse.ErrorMessage);
    }
    
    [Fact]
    public async void DeleteWatchlistItemAsync_WatchlistNotFound()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };


        Watchlist watchlist = new Watchlist();
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistItemAsync(watchlistItemRequestDto);
        
        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.BadRequest, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }

    
    [Fact]
    public async void DeleteWatchlistItemAsync_Exception_IssueSavingWatchlist()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "ALLY",
            SortOrder = 1
        };

        List<WatchlistItem> watchlistItems = new List<WatchlistItem>
        {
            new()
            {
                Symbol = "MSFT",
                SortOrder = 1
            },
            new()
            {
                Symbol = "AAPL",
                SortOrder = 2
            }
        };
        Watchlist watchlist = GetWatchlist();
        watchlist.WatchlistItems = watchlistItems;
        _watchlistRepository.Setup(x => x.FindWatchlist(It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(watchlist);

        _watchlistRepository.Setup(x => x.SaveWatchlist(It.IsAny<Watchlist>())).Throws(new Exception());
        
        WatchlistService watchlistService = new WatchlistService(_watchlistRepository.Object, _mockLogger.Object);
        WatchlistCommandResponse commandResponse = await watchlistService.DeleteWatchlistItemAsync(watchlistItemRequestDto);
        
        Assert.False(commandResponse.Success);
        Assert.Equal(HttpStatusCode.InternalServerError, commandResponse.StatusCode);
        Assert.NotNull(commandResponse.ErrorMessage);
    }
    
    private static WatchlistRequestDto GetWatchlistRequestDto()
    {
        WatchlistRequestDto watchlistRequestDto = new WatchlistRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            NewName = "RENAMEDWATCHLIST"
        };
        return watchlistRequestDto;
    }
    private static List<Watchlist> GetWatchlists()
    {
        List<Watchlist> watchlists = new List<Watchlist>();
        Watchlist watchlist = new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten",
            CreatedBy = "user",
            CreatedDate = DateTime.Now,
            LastUpdatedBy = "user",
            LastUpdatedDate = DateTime.Now,
            WatchlistItems = new List<WatchlistItem>
            {
                new WatchlistItem
                {
                    Symbol = "MSFT",
                    SortOrder = 1
                }
            }
        };
        watchlists.Add(watchlist);
        return watchlists;
    }
    
    private static Watchlist GetWatchlist()
    {
        Watchlist watchlist = new Watchlist
        {
            Name = "NEWWATCHLIST",
            SviUserId = "rkosten",
            CreatedBy = "user",
            CreatedDate = DateTime.Now,
            LastUpdatedBy = "user",
            LastUpdatedDate = DateTime.Now
        };
        return watchlist;
    }
}