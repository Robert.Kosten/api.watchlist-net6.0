using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using api.watchlist_net6.Model;
using api.watchlist_net6.V1.Controllers;
using api.watchlist_net6.V1.Handlers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.V1.Controllers;

public class WatchlistControllerTests
{
    private readonly Mock<IMediator> _mockMediator;
    private readonly Mock<ILogger<WatchlistController>> _mockLogger;

    public WatchlistControllerTests()
    {
        _mockMediator = new Mock<IMediator>();
        _mockLogger = new Mock<ILogger<WatchlistController>>();
    }

    [Fact]
    public async void GetWatchlistsAsync()
    {
        var watchlistDtos = GetWatchlistDtos();
        _mockMediator.Setup(mm => mm.Send(It.IsAny<QueryHandler.WatchlistQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistDtos);


        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var response = await watchlistController
            .GetWatchlistsAsync("rkosten", false);
        var result = (OkObjectResult) response.Result;
        var value = result.Value;

        Assert.Equal(typeof(OkObjectResult), result.GetType());
        Assert.Equal(200, result.StatusCode);
        
        List<WatchlistDto> watchlistDtosResponse = (List<WatchlistDto>)value;
        WatchlistDto responseDto = watchlistDtosResponse[0];
        ValidateWatchlistDto(responseDto);

        IList<WatchlistItemDto> watchlistItemDtos = responseDto.WatchlistItems.ToList();
        WatchlistItemDto watchlistItemDtoResponse = watchlistItemDtos[0];
        ValidateWatchlistItem(watchlistItemDtoResponse);
    }

    [Fact]
    public async void GetWatchlistAsync()
    {
        WatchlistDto watchlistDto = GetWatchlistDto();
        _mockMediator.Setup(mm => mm.Send(It.IsAny<QueryHandler.WatchlistItemQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistDto);
        
        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var response = await watchlistController
            .GetWatchlistAsync("rkosten", "NEWWATCHLIST");
        var result = (OkObjectResult) response.Result;

        WatchlistDto responseDto = (WatchlistDto) result.Value;
        ValidateWatchlistDto(responseDto);
        IList<WatchlistItemDto> watchlistItemDtos = responseDto.WatchlistItems.ToList();
        WatchlistItemDto watchlistItemDtoResponse = watchlistItemDtos[0];
        ValidateWatchlistItem(watchlistItemDtoResponse);
    }

    [Fact]
    public async void CreateWatchlist()
    {
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse
        {
            Success = true,
            StatusCode = HttpStatusCode.Created
        };

        _mockMediator.Setup(x =>
                x.Send(It.IsAny<CommandHandler.WatchlistCreateCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistCommandResponse);
        
        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var watchlistResult = await watchlistController.CreateWatchlistAsync(GetWatchlistRequestDto());
        
        Assert.Equal(typeof(CreatedResult), watchlistResult.GetType());
        var value = ((CreatedResult) watchlistResult).Value;        // Assert.True(value.Success);
        Assert.True(((WatchlistCommandResponse) value).Success);
        Assert.Equal(HttpStatusCode.Created, ((WatchlistCommandResponse) value).StatusCode);
    }
    
    [Fact]
    public async void CreateWatchlistItem()
    {
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse
        {
            Success = true,
            StatusCode = HttpStatusCode.Created
        };

        _mockMediator.Setup(x =>
                x.Send(It.IsAny<CommandHandler.WatchlistItemCreateCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistCommandResponse);
        
        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var watchlistItemAsync = await watchlistController.CreateWatchlistItemAsync(GetWatchlistItemRequestDto());
        
        Assert.Equal(typeof(CreatedResult), watchlistItemAsync.GetType());
        var value = ((CreatedResult) watchlistItemAsync).Value;        // Assert.True(value.Success);
        Assert.True(((WatchlistCommandResponse) value).Success);
        Assert.Equal(HttpStatusCode.Created, ((WatchlistCommandResponse) value).StatusCode);
    }

    [Fact]
    public async void RenameWatchlist()
    {
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse
        {
            Success = true,
            StatusCode = HttpStatusCode.Accepted
        };
        
        _mockMediator.Setup(x =>
                x.Send(It.IsAny<CommandHandler.WatchlistRenameCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistCommandResponse);

        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var renameResponse = await watchlistController.RenameWatchlist(GetWatchlistRequestDto());
        
        Assert.Equal(typeof(CreatedResult), renameResponse.GetType());
        var value = ((CreatedResult) renameResponse).Value;        // Assert.True(value.Success);
        Assert.True(((WatchlistCommandResponse) value).Success);
        Assert.Equal(HttpStatusCode.Accepted, ((WatchlistCommandResponse) value).StatusCode);
    }

    [Fact]
    public async void DeleteWatchlist()
    {
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse
        {
            Success = true,
            StatusCode = HttpStatusCode.Accepted
        };
        
        _mockMediator.Setup(x =>
                x.Send(It.IsAny<CommandHandler.WatchlistDeleteCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistCommandResponse);
        
        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var deletedResponse = await watchlistController.DeleteWatchlist(GetWatchlistRequestDto());
        
        Assert.Equal(typeof(CreatedResult), deletedResponse.GetType());
        var value = ((CreatedResult) deletedResponse).Value;        // Assert.True(value.Success);
        Assert.True(((WatchlistCommandResponse) value).Success);
        Assert.Equal(HttpStatusCode.Accepted, ((WatchlistCommandResponse) value).StatusCode);
    }

    [Fact]
    public async void DeleteWatchlistItem()
    {
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse
        {
            Success = true,
            StatusCode = HttpStatusCode.Accepted
        };
        
        _mockMediator.Setup(x =>
                x.Send(It.IsAny<CommandHandler.WatchlistItemDeleteCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(watchlistCommandResponse);
        
        WatchlistController watchlistController = new WatchlistController(_mockMediator.Object, _mockLogger.Object);
        var deletedItemResponse = await watchlistController.DeleteWatchlistItem(GetWatchlistItemRequestDto());
        
        Assert.Equal(typeof(CreatedResult), deletedItemResponse.GetType());
        var value = ((CreatedResult) deletedItemResponse).Value;        // Assert.True(value.Success);
        Assert.True(((WatchlistCommandResponse) value).Success);
        Assert.Equal(HttpStatusCode.Accepted, ((WatchlistCommandResponse) value).StatusCode);
    }

    private static ICollection<WatchlistDto> GetWatchlistDtos()
    {
        var watchlistDto = GetWatchlistDto();

        ICollection<WatchlistDto> watchlistDtos = new List<WatchlistDto>();
        watchlistDtos.Add(watchlistDto);
        return watchlistDtos;
    }

    private static WatchlistDto GetWatchlistDto()
    {
        WatchlistDto watchlistDto = new WatchlistDto
        {
            SviUserId = "rkosten",
            Name = "NEWWATCHLIST",
            WatchlistItems = new List<WatchlistItemDto>
            {
                new WatchlistItemDto
                {
                    Name = "NEWWATCHLIST",
                    SviUserId = "rkosten",
                    SortOrder = 1,
                    Symbol = "MSFT"
                }
            }
        };
        return watchlistDto;
    }

    private static WatchlistRequestDto GetWatchlistRequestDto()
    {
        return new WatchlistRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten10",
            NewName = "RENAMEDWATCHLIST"
        };
    }

    private static WatchlistItemRequestDto GetWatchlistItemRequestDto()
    {
        return new WatchlistItemRequestDto()
        {
            Name = "NEWWATCHLIST",
            Symbol = "MSFT",
            SortOrder = 1,
            UserId = "rkosten10"
        };
    }
    
    private static void ValidateWatchlistItem(WatchlistItemDto watchlistItemDtoResponse)
    {
        Assert.Equal("rkosten", watchlistItemDtoResponse.SviUserId);
        Assert.Equal("NEWWATCHLIST", watchlistItemDtoResponse.Name);
        Assert.Equal("MSFT", watchlistItemDtoResponse.Symbol);
        Assert.Equal(1, watchlistItemDtoResponse.SortOrder);
    }

    private static void ValidateWatchlistDto(WatchlistDto responseDto)
    {
        Assert.Equal("rkosten", responseDto.SviUserId);
        Assert.Equal("NEWWATCHLIST", responseDto.Name);
    }
}