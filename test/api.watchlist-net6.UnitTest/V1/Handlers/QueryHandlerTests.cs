using System.Collections.Generic;
using System.Linq;
using System.Threading;
using api.watchlist_net6.Model;
using api.watchlist_net6.Services;
using api.watchlist_net6.V1.Handlers;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.V1.Handlers;

public class QueryHandlerTests
{
    private Mock<IWatchlistService> _watchlistService;
    private readonly Mock<ILogger<QueryHandler.WatchlistQueryHandler>> _mockWatchlistLogger;
    private readonly Mock<ILogger<QueryHandler.WatchlistItemQueryHandler>> _mockWatchlistItemLogger;

    public QueryHandlerTests()
    {
        WatchlistDto watchlistDto = GetWatchlistDto();
        List<WatchlistDto> watchlistDtos = new List<WatchlistDto>
        {
            watchlistDto
        };
        _watchlistService = new Mock<IWatchlistService>();
        _mockWatchlistLogger = new Mock<ILogger<QueryHandler.WatchlistQueryHandler>>();
        _mockWatchlistItemLogger = new Mock<ILogger<QueryHandler.WatchlistItemQueryHandler>>();

        _watchlistService.Setup(x => x.RetrieveWatchlistAsync("rkosten", "NEWWATCHLIST"))
            .ReturnsAsync(watchlistDto);
        _watchlistService.Setup(x => x.RetrieveWatchlistsAsync("rkosten")).ReturnsAsync(watchlistDtos);
        _watchlistService.Setup(x => x.RetrieveWatchlistNamesAsync("rkosten")).ReturnsAsync(watchlistDtos);
    }
    
    [Fact]
    public async void HandleWatchlistQuery_FullWatchlist()
    {
        QueryHandler.WatchlistQuery watchlistQuery = new QueryHandler.WatchlistQuery("rkosten", false);
        QueryHandler.WatchlistQueryValidator watchlistQueryValidator = new QueryHandler.WatchlistQueryValidator();
        await watchlistQueryValidator.ValidateAsync(watchlistQuery);

        QueryHandler.WatchlistQueryHandler handler =
            new QueryHandler.WatchlistQueryHandler(_watchlistService.Object, _mockWatchlistLogger.Object);
        IEnumerable<WatchlistDto> watchlistDtos = await handler.Handle(watchlistQuery, CancellationToken.None);
        IList<WatchlistDto> watchlistDtoList = watchlistDtos.ToList();
        WatchlistDto responseDto = watchlistDtoList[0];
        Assert.Equal("NEWWATCHLIST", responseDto.Name);
        Assert.Equal("rkosten", responseDto.SviUserId);

        List<WatchlistItemDto> watchlistItemDtos = responseDto.WatchlistItems.ToList();
        WatchlistItemDto watchlistItemDto = watchlistItemDtos[0];
        Assert.Equal("MSFT", watchlistItemDto.Symbol);
        Assert.Equal(1, watchlistItemDto.SortOrder);
    }

    [Fact]
    public async void HandleWatchlistQuery_NamesOnly()
    {
        QueryHandler.WatchlistQuery watchlistQuery = new QueryHandler.WatchlistQuery("rkosten", true);
        QueryHandler.WatchlistQueryValidator watchlistQueryValidator = new QueryHandler.WatchlistQueryValidator();
        await watchlistQueryValidator.ValidateAsync(watchlistQuery);

        QueryHandler.WatchlistQueryHandler handler =
            new QueryHandler.WatchlistQueryHandler(_watchlistService.Object, _mockWatchlistLogger.Object);
        IEnumerable<WatchlistDto> watchlistDtos = await handler.Handle(watchlistQuery, CancellationToken.None);
        IList<WatchlistDto> watchlistDtoList = watchlistDtos.ToList();
        WatchlistDto responseDto = watchlistDtoList[0];
        Assert.Equal("NEWWATCHLIST", responseDto.Name);
    }
    
    [Fact]
    public async void HandleWatchlistItemQuery()
    {
        QueryHandler.WatchlistItemQuery watchlistItemQuery = new QueryHandler.WatchlistItemQuery("rkosten", "NEWWATCHLIST");
        QueryHandler.WatchlistItemQueryValidator watchlistItemQueryValidator = new QueryHandler.WatchlistItemQueryValidator();
        await watchlistItemQueryValidator.ValidateAsync(watchlistItemQuery);

        QueryHandler.WatchlistItemQueryHandler handler =
            new QueryHandler.WatchlistItemQueryHandler(_watchlistService.Object, _mockWatchlistItemLogger.Object);
        WatchlistDto watchlistDto = await handler.Handle(watchlistItemQuery, CancellationToken.None);
        Assert.Equal("NEWWATCHLIST", watchlistDto.Name);
        Assert.Equal("rkosten", watchlistDto.SviUserId);

        WatchlistItemDto watchlistItemDto = watchlistDto.WatchlistItems.ToList()[0];
        Assert.Equal("MSFT", watchlistItemDto.Symbol);
        Assert.Equal(1, watchlistItemDto.SortOrder);
    }

    private static WatchlistDto GetWatchlistDto()
    {
        return new WatchlistDto
        {
            SviUserId = "rkosten",
            Name = "NEWWATCHLIST",
            WatchlistItems = new List<WatchlistItemDto>
            {
                new WatchlistItemDto
                {
                    SviUserId = "rkosten",
                    Name = "NEWWATCHLIST",
                    Symbol = "MSFT",
                    SortOrder = 1
                }
            }
        };
    }
}