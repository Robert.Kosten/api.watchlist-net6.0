using System;
using System.Net;
using System.Threading;
using api.watchlist_net6.Model;
using api.watchlist_net6.Services;
using api.watchlist_net6.V1.Handlers;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.V1.Handlers;

public class CommandHandlerTests
{
    private Mock<IWatchlistService> _watchlistService;

    public CommandHandlerTests()
    {
        _watchlistService = new Mock<IWatchlistService>();

        _watchlistService.Setup(x => x.SaveWatchlistAsync(It.IsAny<WatchlistRequestDto>()))
            .ReturnsAsync(GetWatchlistCommandResponse());
        _watchlistService.Setup(x => x.SaveWatchlistItemAsync(It.IsAny<WatchlistItemRequestDto>()))
            .ReturnsAsync(GetWatchlistCommandResponse);
        _watchlistService.Setup(x => x.RenameWatchlistAsync(It.IsAny<WatchlistRequestDto>()))
            .ReturnsAsync(GetWatchlistCommandResponse);
        _watchlistService.Setup(x => x.DeleteWatchlistAsync(It.IsAny<WatchlistRequestDto>()))
            .ReturnsAsync(GetWatchlistCommandResponse);
        _watchlistService.Setup(x => x.DeleteWatchlistItemAsync(It.IsAny<WatchlistItemRequestDto>()))
            .ReturnsAsync(GetWatchlistCommandResponse);
    }

    [Fact]
    public async void HandleWatchlistCreate()
    {
        WatchlistRequestDto watchlistRequestDto = new WatchlistRequestDto
        {
            UserId = "rkosten",
            Name = "NEWWATCHLIST"
        };
        CommandHandler.WatchlistCreateCommand watchlistCreateCommand =
            new CommandHandler.WatchlistCreateCommand(watchlistRequestDto);
        CommandHandler.WatchlistCreateCommandValidator validator = new CommandHandler.WatchlistCreateCommandValidator();
        await validator.ValidateAsync(watchlistCreateCommand);

        Mock<ILogger<CommandHandler.WatchlistCreateHandler>> mockLogger =
            new Mock<ILogger<CommandHandler.WatchlistCreateHandler>>();
        CommandHandler.WatchlistCreateHandler handler =
            new CommandHandler.WatchlistCreateHandler(_watchlistService.Object, mockLogger.Object);
        WatchlistCommandResponse response = await handler.Handle(watchlistCreateCommand, CancellationToken.None);

        Assert.True(response.Success);
        Assert.Equal(String.Empty, response.ErrorMessage);
        Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
    }

    [Fact]
    public async void HandleWatchlistItemCreate()
    {
        WatchlistItemRequestDto watchlistItemRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "AAPL",
            SortOrder = 2
        };

        CommandHandler.WatchlistItemCreateCommand watchlistItemCreateCommand =
            new CommandHandler.WatchlistItemCreateCommand(watchlistItemRequestDto);
        CommandHandler.WatchlistItemCreateCommandValidator validator =
            new CommandHandler.WatchlistItemCreateCommandValidator();
        await validator.ValidateAsync(watchlistItemCreateCommand);

        Mock<ILogger<CommandHandler.WatchlistItemCreateHandler>> _mockLogger =
            new Mock<ILogger<CommandHandler.WatchlistItemCreateHandler>>();
        CommandHandler.WatchlistItemCreateHandler handler =
            new CommandHandler.WatchlistItemCreateHandler(_watchlistService.Object, _mockLogger.Object);
        WatchlistCommandResponse response = await handler.Handle(watchlistItemCreateCommand, CancellationToken.None);

        Assert.True(response.Success);
        Assert.Equal(String.Empty, response.ErrorMessage);
        Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
    }

    [Fact]
    public async void HandleWatchlistRename()
    {
        WatchlistRequestDto watchlistRequestDto = new WatchlistRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            NewName = "RENAMEDWATCHLIST"
        };

        CommandHandler.WatchlistRenameCommand watchlistRenameCommand =
            new CommandHandler.WatchlistRenameCommand(watchlistRequestDto);
        CommandHandler.WatchlistRenameCommandValidator validator =
            new CommandHandler.WatchlistRenameCommandValidator();
        await validator.ValidateAsync(watchlistRenameCommand);

        Mock<ILogger<CommandHandler.WatchlistRenameHandler>> _mockLogger =
            new Mock<ILogger<CommandHandler.WatchlistRenameHandler>>();
        CommandHandler.WatchlistRenameHandler handler =
            new CommandHandler.WatchlistRenameHandler(_watchlistService.Object, _mockLogger.Object);
        WatchlistCommandResponse response = await handler.Handle(watchlistRenameCommand, CancellationToken.None);

        Assert.True(response.Success);
        Assert.Equal(String.Empty, response.ErrorMessage);
        Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
    }

    [Fact]
    public async void HandleWatchlistDelete()
    {
        WatchlistRequestDto watchlistRequestDto = new WatchlistRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
        };

        CommandHandler.WatchlistDeleteCommand watchlistDeleteCommand =
            new CommandHandler.WatchlistDeleteCommand(watchlistRequestDto);
        CommandHandler.WatchlistDeleteCommandValidator validator =
            new CommandHandler.WatchlistDeleteCommandValidator();
        await validator.ValidateAsync(watchlistDeleteCommand);

        Mock<ILogger<CommandHandler.WatchlistDeleteHandler>> _mockLogger =
            new Mock<ILogger<CommandHandler.WatchlistDeleteHandler>>();
        CommandHandler.WatchlistDeleteHandler handler =
            new CommandHandler.WatchlistDeleteHandler(_watchlistService.Object, _mockLogger.Object);
        WatchlistCommandResponse response = await handler.Handle(watchlistDeleteCommand, CancellationToken.None);

        Assert.True(response.Success);
        Assert.Equal(String.Empty, response.ErrorMessage);
        Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
    }

    [Fact]
    public async void HandleWatchlistItemDelete()
    {
        WatchlistItemRequestDto watchlistRequestDto = new WatchlistItemRequestDto
        {
            Name = "NEWWATCHLIST",
            UserId = "rkosten",
            Symbol = "MSFT",
            SortOrder = 1
        };

        CommandHandler.WatchlistItemDeleteCommand watchlistItemDeleteCommand =
            new CommandHandler.WatchlistItemDeleteCommand(watchlistRequestDto);
        CommandHandler.WatchlistItemDeleteCommandValidator validator =
            new CommandHandler.WatchlistItemDeleteCommandValidator();
        await validator.ValidateAsync(watchlistItemDeleteCommand);

        Mock<ILogger<CommandHandler.WatchlistItemDeleteHandler>> _mockLogger =
            new Mock<ILogger<CommandHandler.WatchlistItemDeleteHandler>>();
        CommandHandler.WatchlistItemDeleteHandler handler =
            new CommandHandler.WatchlistItemDeleteHandler(_watchlistService.Object, _mockLogger.Object);
        WatchlistCommandResponse response = await handler.Handle(watchlistItemDeleteCommand, CancellationToken.None);

        Assert.True(response.Success);
        Assert.Equal(String.Empty, response.ErrorMessage);
        Assert.Equal(HttpStatusCode.Accepted, response.StatusCode);
    }

    private static WatchlistCommandResponse GetWatchlistCommandResponse()
    {
        return new WatchlistCommandResponse
        {
            Success = true,
            ErrorMessage = "",
            StatusCode = HttpStatusCode.Accepted
        };
    }
}