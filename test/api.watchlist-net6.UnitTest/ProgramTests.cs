using api.watchlist_net6.Services;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Xunit;

namespace api.watchlist_net6.UnitTest;

public class ProgramTests
{
    private readonly TestApplication _app;

    public ProgramTests()
    {
        _app = new TestApplication();
    }

    [Fact]
    public void InitializeProgram()
    {
        var client = _app.CreateDefaultClient();
        var result = client.GetStringAsync("/");
        Assert.NotNull(result);
    }
}

internal class TestApplication : WebApplicationFactory<Program>
{
    protected override IHost CreateHost(IHostBuilder builder)
    {
        builder.ConfigureServices(s => {
            s.AddScoped<IWatchlistService, WatchlistService>();
        });

        return base.CreateHost(builder);
    }
}