using api.watchlist_net6.GraphQL.Types;
using Xunit;

namespace api.watchlist_net6.UnitTest.GraphQL;

public class WatchlistTypeTests
{
    [Fact]
    public void InitializeWatchlistType()
    {
        WatchlistType watchlistType = new WatchlistType();
        Assert.NotNull(watchlistType);
        Assert.NotEmpty(watchlistType.Fields);
    }
}