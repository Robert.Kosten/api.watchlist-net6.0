using System;
using api.watchlist_net6.GraphQL.Queries;
using api.watchlist_net6.GraphQL.Schemas;
using api.watchlist_net6.Repository;
using GraphQL.NewtonsoftJson;
using Moq;
using Xunit;

namespace api.watchlist_net6.UnitTest.GraphQL;

public class WatchlistSchemaTests
{
    private readonly Mock<IServiceProvider> _serviceProvider;

    public WatchlistSchemaTests()
    {
        _serviceProvider = new Mock<IServiceProvider>();
        var watchlistRepository = new Mock<IWatchlistRepository>();
        WatchlistGraphQLQuery query = new WatchlistGraphQLQuery(watchlistRepository.Object);
        _serviceProvider.Setup(x => x.GetService(It.IsAny<Type>())).Returns(query);
    }

    [Fact]
    public void InitializeWatchlistSchema()
    {
        WatchlistSchema watchlistSchema = new WatchlistSchema(_serviceProvider.Object);
        Assert.NotNull(watchlistSchema);
        Assert.NotNull(watchlistSchema.Query);
        watchlistSchema.Query.GetValue();
    }
}