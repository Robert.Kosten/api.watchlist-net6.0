using api.watchlist_net6.GraphQL.Queries;
using api.watchlist_net6.Repository.Entity;
using Xunit;

namespace api.watchlist_net6.UnitTest.GraphQL;

public class QueryTests
{
    [Fact]
    public async void InitializeSimpleQuery()
    {
        Query query = new Query();
        Watchlist watchlist = await query.GetWatchlist("rkosten", "NEWWATCHLIST");
        Assert.NotNull(watchlist);
        Assert.Equal("rkosten", watchlist.SviUserId);
    }
}