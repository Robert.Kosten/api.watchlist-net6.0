using api.watchlist_net6.GraphQL.Types;
using Xunit;

namespace api.watchlist_net6.UnitTest.GraphQL;

public class WatchlistItemTypeTests
{
    [Fact]
    public void InitializeWatchlistItemType()
    {
        WatchlistItemType watchlistItemType = new WatchlistItemType();
        Assert.NotNull(watchlistItemType);
        Assert.NotEmpty(watchlistItemType.Fields);
    }
}