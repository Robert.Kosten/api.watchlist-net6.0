using api.watchlist_net6.Model;

namespace api.watchlist_net6.Services;

public interface IWatchlistService
{
    Task<ICollection<WatchlistDto>> RetrieveWatchlistNamesAsync(string userId);
    Task<ICollection<WatchlistDto>> RetrieveWatchlistsAsync(string userId);
    Task<WatchlistDto> RetrieveWatchlistAsync(string userId, string name);
    Task<WatchlistCommandResponse> SaveWatchlistAsync(WatchlistRequestDto watchlistRequestDto);
    Task<WatchlistCommandResponse> SaveWatchlistItemAsync(WatchlistItemRequestDto watchlistItemRequestDto);
    Task<WatchlistCommandResponse> RenameWatchlistAsync(WatchlistRequestDto watchlistRequestDto);
    Task<WatchlistCommandResponse> DeleteWatchlistAsync(WatchlistRequestDto watchlistRequestDto);
    Task<WatchlistCommandResponse> DeleteWatchlistItemAsync(WatchlistItemRequestDto watchlistItemRequestDto);
}