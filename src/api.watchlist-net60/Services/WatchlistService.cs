using System.Net;
using api.watchlist_net6.Model;
using api.watchlist_net6.Repository;
using api.watchlist_net6.Repository.Entity;

namespace api.watchlist_net6.Services;

public class WatchlistService : IWatchlistService
{
    private readonly IWatchlistRepository _watchlistRepository;
    private readonly ILogger<WatchlistService> _logger;

    public WatchlistService(IWatchlistRepository watchlistRepository, ILogger<WatchlistService> logger)
    {
        _watchlistRepository = watchlistRepository;
        _logger = logger;
    }

    public async Task<ICollection<WatchlistDto>> RetrieveWatchlistNamesAsync(string userId)
    {
        _logger.LogInformation("Retrieving watchlist names for user {UserId}", userId);

        ICollection<Watchlist> watchlists = await _watchlistRepository.FindAllWatchlistsByUserAsync(userId);

        _logger.LogInformation("Number of watchlists found for user {UserId}: {Count}", userId, watchlists.Count);

        ICollection<WatchlistDto> watchlistDtos = watchlists.Select(x => new WatchlistDto
        {
            Name = x.Name
        }).ToList();

        return watchlistDtos;
    }

    public async Task<ICollection<WatchlistDto>> RetrieveWatchlistsAsync(string userId)
    {
        _logger.LogInformation("Retrieving watchlists for user {UserId}", userId);

        ICollection<Watchlist> watchlists = await _watchlistRepository.FindAllWatchlistsByUserAsync(userId);

        _logger.LogInformation("Number of watchlists found for user {UserId}: {Count}", userId, watchlists.Count);
        
        ICollection<WatchlistDto> watchlistDtos = watchlists.Select(x => new WatchlistDto
        {
            SviUserId = x.SviUserId,
            Name = x.Name,
            WatchlistItems = x.WatchlistItems.Select(y => new WatchlistItemDto
            {
                Symbol = y.Symbol,
                SortOrder = y.SortOrder,
            }).ToList()
        }).ToList();

        return watchlistDtos;
    }

    public async Task<WatchlistDto> RetrieveWatchlistAsync(string userId, string name)
    {
        _logger.LogInformation("Searching for single watchlist for user {UserId} and name {Name}", userId, name);
        Watchlist watchlist = await _watchlistRepository.FindWatchlist(userId, name);

        _logger.LogInformation("Found watchlist for user and name {Watchlist}", watchlist?.SviUserId != null);
        return new WatchlistDto
        {
            SviUserId = watchlist.SviUserId,
            Name = watchlist.Name,
            WatchlistItems = watchlist.WatchlistItems.Select(y => new WatchlistItemDto
            {
                Symbol = y.Symbol,
                SortOrder = y.SortOrder,
            }).ToList()
        };
    }

    public async Task<WatchlistCommandResponse> SaveWatchlistAsync(WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation(
            "Creating watchlist for user {UserId} and name {Name}", watchlistRequestDto.UserId,
            watchlistRequestDto.Name);
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse();

        // watchlist should not exist
        Watchlist oldWatchlist =
            await _watchlistRepository.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name);

        if (oldWatchlist?.SviUserId != null)
        {
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist exists for user {watchlistRequestDto.UserId} and name {watchlistRequestDto.Name}";

            return watchlistCommandResponse;
        }

        DateTime dateTime = DateTime.Now;
        Watchlist watchlist = new Watchlist
        {
            SviUserId = watchlistRequestDto.UserId,
            Name = watchlistRequestDto.Name,
            CreatedBy = "user",
            CreatedDate = dateTime,
            LastUpdatedBy = "user",
            LastUpdatedDate = dateTime
        };

        try
        {
            await _watchlistRepository.SaveWatchlist(watchlist);
            watchlistCommandResponse.Success = true;
            watchlistCommandResponse.StatusCode = HttpStatusCode.Created;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Saving watchlist result in {Message}", exception.Message);
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.InternalServerError;
            watchlistCommandResponse.ErrorMessage = exception.Message;
        }

        return watchlistCommandResponse;
    }

    public async Task<WatchlistCommandResponse> SaveWatchlistItemAsync(WatchlistItemRequestDto watchlistItemRequestDto)
    {
        _logger.LogInformation(
            "Updating watchlist item for user {UserId} and name {Name}", watchlistItemRequestDto.UserId,
            watchlistItemRequestDto.Name);

        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse();

        Watchlist watchlist =
            await _watchlistRepository.FindWatchlist(watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name);

        if (watchlist?.SviUserId == null)
        {
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist does not exist for user {watchlistItemRequestDto.UserId} and name {watchlistItemRequestDto.Name}";

            return watchlistCommandResponse;
        }

        List<WatchlistItem> watchlistItems = watchlist.WatchlistItems;
        if (watchlistItems == null || !watchlistItems.Any())
        {
            watchlistItems = new List<WatchlistItem>();
        }

        if (watchlistItems.Any(wi => watchlistItemRequestDto.Symbol == wi.Symbol))
        {
            watchlistItems.Find(wi => watchlistItemRequestDto.Symbol == wi.Symbol)!.SortOrder =
                watchlistItemRequestDto.SortOrder;
        }
        else
        {
            watchlistItems.Add(new WatchlistItem
            {
                Symbol = watchlistItemRequestDto.Symbol,
                SortOrder = watchlistItemRequestDto.SortOrder
            });
        }

        watchlist.WatchlistItems = watchlistItems;
        watchlist.LastUpdatedBy = "user";
        watchlist.LastUpdatedDate = DateTime.Now;

        try
        {
            await _watchlistRepository.SaveWatchlist(watchlist);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error updating watchlist for user {UserId}", watchlistItemRequestDto.UserId);

            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.InternalServerError;
            watchlistCommandResponse.ErrorMessage = $"Exception when updating watchlist {ex.Message}";
            return watchlistCommandResponse;
        }

        watchlistCommandResponse.Success = true;
        watchlistCommandResponse.StatusCode = HttpStatusCode.Accepted;

        return watchlistCommandResponse;
    }

    public async Task<WatchlistCommandResponse> RenameWatchlistAsync(WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation("Renaming watchlist from {Name} to {NewName} for user {UserId}",
            watchlistRequestDto.Name, watchlistRequestDto.NewName, watchlistRequestDto.UserId);

        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse();

        // you should have a watchlist to change the name of ...
        Watchlist watchlist =
            await _watchlistRepository.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name);

        if (watchlist?.SviUserId == null)
        {
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist does not exist for user {watchlistRequestDto.UserId} and name {watchlistRequestDto.Name}";

            return watchlistCommandResponse;
        }

        // now check that a watchlist with the new name does not already exist
        Watchlist watchlistWithNewName =
            await _watchlistRepository.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.NewName);

        if (watchlistWithNewName?.SviUserId != null)
        {
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist already exists for user {watchlistRequestDto.UserId} and new name {watchlistRequestDto.NewName}";

            return watchlistCommandResponse;
        }

        // DynamoDB will create a new record when you rename a key value
        // Save watchlist with new name first
        watchlist.Name = watchlistRequestDto.NewName;
        watchlist.LastUpdatedDate = DateTime.Now;

        try
        {
            await _watchlistRepository.SaveWatchlist(watchlist);

            // now delete the old watchlist
            Watchlist oldWatchlist = new Watchlist
            {
                SviUserId = watchlistRequestDto.UserId,
                Name = watchlistRequestDto.Name
            };
            await _watchlistRepository.DeleteWatchlist(oldWatchlist);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Error renaming watchlist for user {UserId}", watchlistRequestDto.UserId);

            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.InternalServerError;
            watchlistCommandResponse.ErrorMessage = $"Exception when renaming watchlist {exception.Message}";
            return watchlistCommandResponse;
        }

        watchlistCommandResponse.Success = true;
        watchlistCommandResponse.StatusCode = HttpStatusCode.Accepted;

        return watchlistCommandResponse;
    }

    public async Task<WatchlistCommandResponse> DeleteWatchlistAsync(WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation(
            "Deleting watchlist for user {UserId} and name {Name}", watchlistRequestDto.UserId,
            watchlistRequestDto.Name);
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse();

        // you should have a watchlist to delete ...
        Watchlist watchlist =
            await _watchlistRepository.FindWatchlist(watchlistRequestDto.UserId, watchlistRequestDto.Name);

        if (watchlist?.SviUserId == null)
        {
            _logger.LogError(
                "Error: deleting watchlist that does not exist for user {UserId} and name {Name}",
                watchlistRequestDto.UserId, watchlistRequestDto.Name);
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist does not exist for user {watchlistRequestDto.UserId} and name {watchlistRequestDto.Name}";

            return watchlistCommandResponse;
        }

        try
        {
            await _watchlistRepository.DeleteWatchlist(watchlist);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Error deleting watchlist for user {UserId}", watchlistRequestDto.UserId);

            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.InternalServerError;
            watchlistCommandResponse.ErrorMessage = $"Exception when deleting watchlist {exception.Message}";
            return watchlistCommandResponse;
        }

        watchlistCommandResponse.Success = true;
        watchlistCommandResponse.StatusCode = HttpStatusCode.Accepted;

        return watchlistCommandResponse;
    }

    public async Task<WatchlistCommandResponse> DeleteWatchlistItemAsync(
        WatchlistItemRequestDto watchlistItemRequestDto)
    {
        _logger.LogInformation("Deleting watchlist item for user {UserId} and name {Name} and symbol {Symbol}",
            watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name, watchlistItemRequestDto.Symbol);
        WatchlistCommandResponse watchlistCommandResponse = new WatchlistCommandResponse();

        // you should have a watchlist to delete from ...
        Watchlist watchlist =
            await _watchlistRepository.FindWatchlist(watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name);

        if (watchlist?.SviUserId == null)
        {
            _logger.LogError(
                "Error: deleting watchlist item from watchlist that does not exist for user {UserId} and name {Name} and symbol {Symbol}",
                watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name, watchlistItemRequestDto.Symbol);
            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.BadRequest;
            watchlistCommandResponse.ErrorMessage =
                $"Watchlist does not exist for user {watchlistItemRequestDto.UserId} and name {watchlistItemRequestDto.Name}";

            return watchlistCommandResponse;
        }

        List<WatchlistItem> watchlistItems = watchlist.WatchlistItems;
        // ok ... if there are no watchlist items to delete ... no harm, no foul
        if (watchlistItems == null || !watchlistItems.Any())
        {
            _logger.LogInformation("No watchlist items to delete for user {UserId} " +
                                   "and name {Name}", watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name);
            watchlistCommandResponse.Success = true;
            watchlistCommandResponse.StatusCode = HttpStatusCode.Accepted;
            return watchlistCommandResponse;
        }

        // if you try to remove a symbol that does not exist in the list, then no harm, no foul
        if (watchlistItems.Any(wi => watchlistItemRequestDto.Symbol == wi.Symbol))
        {
            _logger.LogInformation("Removing watchlist item for user {UserId} and name {Name} and symbol {Symbol}",
                watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name, watchlistItemRequestDto.Symbol);
            watchlistItems.RemoveAll(wi => watchlistItemRequestDto.Symbol == wi.Symbol);
            watchlist.LastUpdatedDate = DateTime.Now;
        }

        try
        {
            await _watchlistRepository.SaveWatchlist(watchlist);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error deleting watchlist item for user {UserId} and name {Name} and symbol {Symbol}",
                watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name, watchlistItemRequestDto.Symbol);

            watchlistCommandResponse.Success = false;
            watchlistCommandResponse.StatusCode = HttpStatusCode.InternalServerError;
            watchlistCommandResponse.ErrorMessage = $"Exception when deleting watchlist item {ex.Message}";
            return watchlistCommandResponse;
        }

        // item deleted
        watchlistCommandResponse.Success = true;
        watchlistCommandResponse.StatusCode = HttpStatusCode.Accepted;

        return watchlistCommandResponse;
    }
}