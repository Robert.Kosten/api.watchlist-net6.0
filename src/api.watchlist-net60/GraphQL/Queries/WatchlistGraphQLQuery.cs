using api.watchlist_net6.GraphQL.Types;
using api.watchlist_net6.Repository;
using GraphQL;
using GraphQL.Types;

namespace api.watchlist_net6.GraphQL.Queries;

public class WatchlistGraphQLQuery : ObjectGraphType
{
    public WatchlistGraphQLQuery(IWatchlistRepository repository)
    {
        Field<ListGraphType<WatchlistType>>(
            "watchlists",
            arguments: new QueryArguments(new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "sviUserId" }),
            resolve: context =>
            {
                var sviUserId = context.GetArgument<string>("sviUserId");
                return repository.FindAllWatchlistsByUserAsync(sviUserId);
            });
    }
}