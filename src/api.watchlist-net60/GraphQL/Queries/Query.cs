using api.watchlist_net6.Repository;
using api.watchlist_net6.Repository.Entity;
using GraphQL;
using GraphQL.Types;

namespace api.watchlist_net6.GraphQL.Queries;

public class Query
{
    [GraphQLMetadata("watchlist")]
    public async Task<Watchlist> GetWatchlist(string sviUserId, string name)
    {
        return new Watchlist
        {
            SviUserId = sviUserId,
            Name = name
        };
    }
}