using api.watchlist_net6.GraphQL.Queries;
using GraphQL.Types;

namespace api.watchlist_net6.GraphQL.Schemas;

public class WatchlistSchema : Schema
{
    public WatchlistSchema(IServiceProvider provider)
        : base(provider)
    {
        Query = provider.GetRequiredService<WatchlistGraphQLQuery>();
    }
}