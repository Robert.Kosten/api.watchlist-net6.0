using api.watchlist_net6.Repository.Entity;
using GraphQL.Types;

namespace api.watchlist_net6.GraphQL.Types;

public sealed class WatchlistItemType : ObjectGraphType<WatchlistItem>
{
    public WatchlistItemType()
    {
        Field(x => x.Symbol).Description("Market symbol.");
        Field(x => x.SortOrder, nullable: true, type: typeof(IntGraphType))
            .Description("UI sort order based on user preference.");
    }
}