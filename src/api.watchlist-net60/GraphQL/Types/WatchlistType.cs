using api.watchlist_net6.Repository.Entity;
using GraphQL.Types;

namespace api.watchlist_net6.GraphQL.Types;

public sealed class WatchlistType : ObjectGraphType<Watchlist>
{
    public WatchlistType()
    {
        Field(x => x.SviUserId).Description("Hash key and id for user of watchlist.");
        Field(x => x.Name).Description("Sort key and Name of the watchlist.");
        Field(x => x.CreatedBy).Description("Creator of watchlist.");
        Field(x => x.CreatedDate).Description("Created date watchlist.");
        Field(x => x.LastUpdatedBy).Description("User who last updated watchlist.");
        Field(x => x.LastUpdatedDate).Description("Date watchlist was last updated");
        Field<ListGraphType<WatchlistItemType>>("watchlistItems");
    }
}