using api.watchlist_net6.Model;
using api.watchlist_net6.V1.Handlers;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace api.watchlist_net6.V1.Controllers;

[ApiController]
[Route("/v1/customers")]
[Produces("application/json")]
public class WatchlistController : Controller
{
    private readonly IMediator _mediator;
    private readonly ILogger<WatchlistController> _logger;

    public WatchlistController(IMediator mediator, ILogger<WatchlistController> logger)
    {
        _mediator = mediator;
        _logger = logger;
    }

    [HttpGet("{userId}/watchlists")]
    [ProducesResponseType(typeof(WatchlistDto), 200)]
    [ProducesResponseType(500)]
    public async Task<ActionResult<IEnumerable<WatchlistDto>>> GetWatchlistsAsync(string userId,
        [FromQuery] bool namesOnly = false)
    {
        _logger.LogInformation("Searching for watchlists for user {UserId}", userId);
        return Ok(await _mediator.Send(new QueryHandler.WatchlistQuery(userId, namesOnly)));
    }

    [HttpGet("{userId}/watchlists/{name}/symbols")]
    [ProducesResponseType(typeof(WatchlistDto), 200)]
    [ProducesResponseType(500)]
    public async Task<ActionResult<WatchlistDto>> GetWatchlistAsync(string userId, string name)
    {
        _logger.LogInformation("Searching for watchlist for user {UserId} and name {Name}", userId, name);
        return Ok(await _mediator.Send(new QueryHandler.WatchlistItemQuery(userId, name)));
    }

    [HttpPost("watchlists")]
    [ProducesResponseType(typeof(WatchlistCommandResponse), 201)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> CreateWatchlistAsync([FromBody] WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation("Creating watchlist for user {UserId} and name {Name}", watchlistRequestDto.UserId,
            watchlistRequestDto.Name);
        return Created("GetOne", await _mediator.Send(new CommandHandler.WatchlistCreateCommand(watchlistRequestDto)));
    }

    [HttpPost("watchlists/symbols")]
    [ProducesResponseType(typeof(WatchlistCommandResponse), 201)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> CreateWatchlistItemAsync([FromBody] WatchlistItemRequestDto watchlistItemRequestDto)
    {
        _logger.LogInformation("Creating watchlist item for user {UserId} and " +
                               "name {Name}", watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name);

        return Created("GetOne",
            await _mediator.Send(new CommandHandler.WatchlistItemCreateCommand(watchlistItemRequestDto)));
    }

    [HttpPost("watchlists/names")]
    [ProducesResponseType(typeof(WatchlistCommandResponse), 201)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> RenameWatchlist([FromBody] WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation("Renaming watchlist for user {UserId} and name {Name} to {NewName}",
            watchlistRequestDto.UserId, watchlistRequestDto.Name, watchlistRequestDto.NewName);
        return Created("GetOne",
            await _mediator.Send(new CommandHandler.WatchlistRenameCommand(watchlistRequestDto)));
    }

    [HttpDelete("watchlists")]
    [ProducesResponseType(typeof(WatchlistCommandResponse), 201)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> DeleteWatchlist([FromBody] WatchlistRequestDto watchlistRequestDto)
    {
        _logger.LogInformation("Deleting watchlist for user {UserId} and name {Name}", watchlistRequestDto.UserId,
            watchlistRequestDto.Name);
        return Created("GetOne",
            await _mediator.Send(new CommandHandler.WatchlistDeleteCommand(watchlistRequestDto)));
    }

    [HttpDelete("watchlists/symbols")]
    [ProducesResponseType(typeof(WatchlistCommandResponse), 201)]
    [ProducesResponseType(500)]
    public async Task<IActionResult> DeleteWatchlistItem([FromBody] WatchlistItemRequestDto watchlistItemRequestDto)
    {
        _logger.LogInformation("Deleting watchlist item for user {UserId} and name {Name} and symbol {Symbol}",
            watchlistItemRequestDto.UserId, watchlistItemRequestDto.Name, watchlistItemRequestDto.Symbol);
        return Created("GetOne",
            await _mediator.Send(new CommandHandler.WatchlistItemDeleteCommand(watchlistItemRequestDto)));
    }
}