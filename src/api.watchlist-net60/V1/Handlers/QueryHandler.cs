using api.watchlist_net6.Model;
using api.watchlist_net6.Services;
using FluentValidation;
using MediatR;

namespace api.watchlist_net6.V1.Handlers;

public class QueryHandler
{
    public class WatchlistQuery : IRequest<IEnumerable<WatchlistDto>>
    {
        public WatchlistQuery(string userId, bool namesOnly)
        {
            UserId = userId;
            NamesOnly = namesOnly;
        }

        public string UserId { get; set; }
        public bool NamesOnly { get; set; }
    }

    public class WatchlistItemQuery : IRequest<WatchlistDto>
    {
        public WatchlistItemQuery(string userId, string name)
        {
            UserId = userId;
            Name = name;
        }

        public string UserId { get; set; }
        public string Name { get; set; }
    }

    public class WatchlistQueryValidator : AbstractValidator<WatchlistQuery>
    {
        public WatchlistQueryValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
        }
    }

    public class WatchlistItemQueryValidator : AbstractValidator<WatchlistItemQuery>
    {
        public WatchlistItemQueryValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
        }
    }

    public class WatchlistQueryHandler : IRequestHandler<WatchlistQuery, IEnumerable<WatchlistDto>>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistQueryHandler> _logger;

        public WatchlistQueryHandler(IWatchlistService watchlistService, ILogger<WatchlistQueryHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<IEnumerable<WatchlistDto>> Handle(WatchlistQuery request,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation("Querying watchlists for user {UserId}", request.UserId);

            if (request.NamesOnly)
            {
                _logger.LogInformation("Looking for watchlist names for user {UserId}", request.UserId);
                return await _watchlistService.RetrieveWatchlistNamesAsync(request.UserId);
            }

            _logger.LogInformation("Looking for watchlists and items for user {UserId}", request.UserId);
            return await _watchlistService.RetrieveWatchlistsAsync(request.UserId);
        }
    }

    public class WatchlistItemQueryHandler : IRequestHandler<WatchlistItemQuery, WatchlistDto>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistItemQueryHandler> _logger;

        public WatchlistItemQueryHandler(IWatchlistService watchlistService, ILogger<WatchlistItemQueryHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }


        public async Task<WatchlistDto> Handle(WatchlistItemQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Looking for watchlist for user {UserId} and name {Name}", request.UserId,
                request.Name);
            return await _watchlistService.RetrieveWatchlistAsync(request.UserId, request.Name);
        }
    }
}