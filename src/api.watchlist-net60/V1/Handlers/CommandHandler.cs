using api.watchlist_net6.Model;
using api.watchlist_net6.Services;
using FluentValidation;
using MediatR;

namespace api.watchlist_net6.V1.Handlers;

public class CommandHandler
{
    public class WatchlistCreateCommand : IRequest<WatchlistCommandResponse>
    {
        public WatchlistCreateCommand(WatchlistRequestDto watchlistRequestDto)
        {
            WatchlistRequestDto = watchlistRequestDto;
        }

        public WatchlistRequestDto WatchlistRequestDto { get; set; }
    }

    public class WatchlistItemCreateCommand : IRequest<WatchlistCommandResponse>
    {
        public WatchlistItemCreateCommand(WatchlistItemRequestDto watchlistItemRequestDto)
        {
            WatchlistItemRequestDto = watchlistItemRequestDto;
        }

        public WatchlistItemRequestDto WatchlistItemRequestDto { get; set; }
    }

    public class WatchlistRenameCommand : IRequest<WatchlistCommandResponse>
    {
        public WatchlistRenameCommand(WatchlistRequestDto watchlistRequestDto)
        {
            WatchlistRequestDto = watchlistRequestDto;
        }

        public WatchlistRequestDto WatchlistRequestDto { get; set; }
    }

    public class WatchlistDeleteCommand : IRequest<WatchlistCommandResponse>
    {
        public WatchlistDeleteCommand(WatchlistRequestDto watchlistRequestDto)
        {
            WatchlistRequestDto = watchlistRequestDto;
        }

        public WatchlistRequestDto WatchlistRequestDto { get; set; }
    }

    public class WatchlistItemDeleteCommand : IRequest<WatchlistCommandResponse>
    {
        public WatchlistItemDeleteCommand(WatchlistItemRequestDto watchlistItemRequestDto)
        {
            WatchlistItemRequestDto = watchlistItemRequestDto;
        }

        public WatchlistItemRequestDto WatchlistItemRequestDto { get; set; }
    }

    public class WatchlistCreateCommandValidator : AbstractValidator<WatchlistCreateCommand>
    {
        public WatchlistCreateCommandValidator()
        {
            RuleFor(x => x.WatchlistRequestDto).NotNull().SetValidator(new WatchlistCreateDataValidator());
        }
    }

    public class WatchlistItemCreateCommandValidator : AbstractValidator<WatchlistItemCreateCommand>
    {
        public WatchlistItemCreateCommandValidator()
        {
            RuleFor(x => x.WatchlistItemRequestDto).NotNull().SetValidator(new WatchlistItemCreateDataValidator());
        }
    }

    public class WatchlistRenameCommandValidator : AbstractValidator<WatchlistRenameCommand>
    {
        public WatchlistRenameCommandValidator()
        {
            RuleFor(x => x.WatchlistRequestDto).NotNull().SetValidator(new WatchlistRenameDataValidator());
        }
    }

    public class WatchlistDeleteCommandValidator : AbstractValidator<WatchlistDeleteCommand>
    {
        public WatchlistDeleteCommandValidator()
        {
            RuleFor(x => x.WatchlistRequestDto).NotNull().SetValidator(new WatchlistDeleteDataValidator());
        }
    }

    public class WatchlistItemDeleteCommandValidator : AbstractValidator<WatchlistItemDeleteCommand>
    {
        public WatchlistItemDeleteCommandValidator()
        {
            RuleFor(x => x.WatchlistItemRequestDto).NotNull().SetValidator(new WatchlistItemDeleteDataValidator());
        }
    }

    public class WatchlistCreateDataValidator : AbstractValidator<WatchlistRequestDto>
    {
        public WatchlistCreateDataValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
        }
    }

    public class WatchlistItemCreateDataValidator : AbstractValidator<WatchlistItemRequestDto>
    {
        public WatchlistItemCreateDataValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Symbol).NotNull().NotEmpty();
        }
    }

    public class WatchlistRenameDataValidator : AbstractValidator<WatchlistRequestDto>
    {
        public WatchlistRenameDataValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.NewName).NotNull().NotEmpty();
        }
    }

    public class WatchlistDeleteDataValidator : AbstractValidator<WatchlistRequestDto>
    {
        public WatchlistDeleteDataValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
        }
    }

    public class WatchlistItemDeleteDataValidator : AbstractValidator<WatchlistItemRequestDto>
    {
        public WatchlistItemDeleteDataValidator()
        {
            RuleFor(x => x.UserId).NotNull().NotEmpty();
            RuleFor(x => x.Name).NotNull().NotEmpty();
            RuleFor(x => x.Symbol).NotNull().NotEmpty();
        }
    }

    public class WatchlistCreateHandler : IRequestHandler<WatchlistCreateCommand, WatchlistCommandResponse>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistCreateHandler> _logger;

        public WatchlistCreateHandler(IWatchlistService watchlistService, ILogger<WatchlistCreateHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<WatchlistCommandResponse> Handle(WatchlistCreateCommand createCommand,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Creating watchlist for user {UserId} and name {Name}", createCommand.WatchlistRequestDto.UserId,
                createCommand.WatchlistRequestDto.Name);

            return await _watchlistService.SaveWatchlistAsync(createCommand.WatchlistRequestDto);
        }
    }

    public class WatchlistItemCreateHandler : IRequestHandler<WatchlistItemCreateCommand, WatchlistCommandResponse>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistItemCreateHandler> _logger;

        public WatchlistItemCreateHandler(IWatchlistService watchlistService,
            ILogger<WatchlistItemCreateHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<WatchlistCommandResponse> Handle(WatchlistItemCreateCommand createCommand,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Creating watchlist for user {UserId} and name {Name}", createCommand.WatchlistItemRequestDto.UserId,
                createCommand.WatchlistItemRequestDto.Name);

            return await _watchlistService.SaveWatchlistItemAsync(createCommand.WatchlistItemRequestDto);
        }
    }

    public class WatchlistRenameHandler : IRequestHandler<WatchlistRenameCommand, WatchlistCommandResponse>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistRenameHandler> _logger;

        public WatchlistRenameHandler(IWatchlistService watchlistService, ILogger<WatchlistRenameHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<WatchlistCommandResponse> Handle(WatchlistRenameCommand request,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Renaming watchlist {UserId} and name {Name} to new name {NewName}", request.WatchlistRequestDto.UserId,
                request.WatchlistRequestDto.Name, request.WatchlistRequestDto.NewName);

            return await _watchlistService.RenameWatchlistAsync(request.WatchlistRequestDto);
        }
    }

    public class WatchlistDeleteHandler : IRequestHandler<WatchlistDeleteCommand, WatchlistCommandResponse>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistDeleteHandler> _logger;

        public WatchlistDeleteHandler(IWatchlistService watchlistService, ILogger<WatchlistDeleteHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<WatchlistCommandResponse> Handle(WatchlistDeleteCommand request,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Deleting watchlist {UserId} and name {Name}", request.WatchlistRequestDto.UserId,
                request.WatchlistRequestDto.Name);

            return await _watchlistService.DeleteWatchlistAsync(request.WatchlistRequestDto);
        }
    }

    public class WatchlistItemDeleteHandler : IRequestHandler<WatchlistItemDeleteCommand, WatchlistCommandResponse>
    {
        private readonly IWatchlistService _watchlistService;
        private readonly ILogger<WatchlistItemDeleteHandler> _logger;

        public WatchlistItemDeleteHandler(IWatchlistService watchlistService,
            ILogger<WatchlistItemDeleteHandler> logger)
        {
            _watchlistService = watchlistService;
            _logger = logger;
        }

        public async Task<WatchlistCommandResponse> Handle(WatchlistItemDeleteCommand request,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Deleting watchlist item {Symbol} for {UserId} and name {Name}", request.WatchlistItemRequestDto.Symbol,
                request.WatchlistItemRequestDto.UserId, request.WatchlistItemRequestDto.Name);

            return await _watchlistService.DeleteWatchlistItemAsync(request.WatchlistItemRequestDto);
        }
    }
}