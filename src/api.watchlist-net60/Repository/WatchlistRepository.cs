using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using api.watchlist_net6.Repository.Entity;

namespace api.watchlist_net6.Repository;

public class WatchlistRepository : IWatchlistRepository
{
    private readonly IDynamoDBContext _context;

    public WatchlistRepository(IDynamoDBContext context)
    {
        _context = context;
    }


    public async Task<ICollection<Watchlist>> FindAllWatchlistsByUserAsync(string sviUserId)
    {
        QueryFilter filter = new QueryFilter();
        filter.AddCondition("sviUserId", QueryOperator.Equal, sviUserId);

        QueryOperationConfig config = new QueryOperationConfig()
        {
            Filter = filter
        };

        AsyncSearch<Watchlist> search = _context.FromQueryAsync<Watchlist>(config);

        return search != null ? await search.GetRemainingAsync() : new List<Watchlist>();
    }

    public async Task<Watchlist> FindWatchlist(string userId, string name)
    {
        return await _context.LoadAsync<Watchlist>(userId, name);
    }

    public async Task SaveWatchlist(Watchlist watchlist)
    {
        await _context.SaveAsync(watchlist);
    }

    public async Task DeleteWatchlist(Watchlist watchlist)
    {
        await _context.DeleteAsync(watchlist);
    }
}