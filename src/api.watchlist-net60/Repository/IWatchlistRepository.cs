using api.watchlist_net6.Repository.Entity;

namespace api.watchlist_net6.Repository;

public interface IWatchlistRepository
{
    Task<ICollection<Watchlist>> FindAllWatchlistsByUserAsync(string userId);
    Task<Watchlist> FindWatchlist(string userId, string name);

    Task SaveWatchlist(Watchlist watchlist);
    Task DeleteWatchlist(Watchlist watchlist);
}