using System.Globalization;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;

namespace api.watchlist_net6.Repository;

public class DateConverter : IPropertyConverter
{
    public object FromEntry(DynamoDBEntry entry)
    {
        var dateTime = entry?.AsString();
        string format = "yyyy-MM-dd'T'HH:mm:ss:fff'Z'";
        if (string.IsNullOrEmpty(dateTime))
            return null;
        if (!DateTime.TryParseExact(dateTime, format, CultureInfo.InvariantCulture, DateTimeStyles.RoundtripKind,
                out DateTime value))
            return null;
        return value;
    }

    public DynamoDBEntry ToEntry(object value)
    {
        if (value == null)
            return new DynamoDBNull();
        if (value.GetType() != typeof(DateTime) && value.GetType() != typeof(DateTime?))
            throw new ArgumentException("value parameter must be a DateTime or a Nullable<DateTime>.",
                nameof(value));
        return ((DateTime)value).ToString();
    }
}