using Amazon.DynamoDBv2.DataModel;

namespace api.watchlist_net6.Repository.Entity;

[Serializable]
public class WatchlistItem
{
    [DynamoDBProperty("symbol")] 
    public string Symbol { get; set; }

    [DynamoDBProperty("sortOrder")] 
    public int? SortOrder { get; set; }
}