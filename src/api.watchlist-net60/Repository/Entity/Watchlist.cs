using Amazon.DynamoDBv2.DataModel;

namespace api.watchlist_net6.Repository.Entity;

[DynamoDBTable("Watchlist")]
[Serializable]
public class Watchlist
{
    [DynamoDBHashKey("sviUserId")]
    [DynamoDBProperty("sviUserId")]
    public string SviUserId { get; set; }

    [DynamoDBRangeKey("name")]
    [DynamoDBProperty("name")]
    public string Name { get; set; }

    [DynamoDBProperty("createdBy")] 
    public string CreatedBy { get; set; }

    [DynamoDBProperty("created", typeof(DateConverter))]
    public DateTime CreatedDate { get; set; }

    [DynamoDBProperty("lastUpdatedBy")] 
    public string LastUpdatedBy { get; set; }

    [DynamoDBProperty("lastUpdated", typeof(DateConverter))]
    public DateTime LastUpdatedDate { get; set; }

    [DynamoDBProperty("watchlistItems")] 
    public List<WatchlistItem> WatchlistItems { get; set; }
}