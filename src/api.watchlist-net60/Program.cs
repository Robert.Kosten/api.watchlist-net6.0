using System.Text.Json.Serialization;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using api.watchlist_net6.GraphQL.Queries;
using api.watchlist_net6.GraphQL.Schemas;
using api.watchlist_net6.Repository;
using api.watchlist_net6.Services;
using api.watchlist_net6.V1.Handlers;
using GraphQL.MicrosoftDI;
using GraphQL.Server;
using GraphQL.Types;
using MediatR;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);
ConfigurationManager configuration = builder.Configuration;

// Add services to the container.
builder.Services.AddSingleton<IWatchlistService, WatchlistService>();
builder.Services.AddSingleton<IWatchlistRepository, WatchlistRepository>();
builder.Services.AddDefaultAWSOptions(configuration.GetAWSOptions());

// set up dynamodb
var dynamoDbConfig = configuration.GetSection("DynamoDb");
var isDevelopmentDynamoDbEnvironment = dynamoDbConfig.GetValue<bool>("LocalMode");
if (isDevelopmentDynamoDbEnvironment)
{
    builder.Services.AddSingleton<IDynamoDBContext>(_ =>
        {
            var clientConfig = new AmazonDynamoDBConfig
                { ServiceURL = dynamoDbConfig.GetValue<string>("LocalServiceUrl") };
            return new DynamoDBContext(new AmazonDynamoDBClient(clientConfig));
        }
    );
}
else
{
    builder.Services.AddSingleton<IDynamoDBContext>(_ =>
        {
            var clientConfig = new AmazonDynamoDBConfig();
            return new DynamoDBContext(new AmazonDynamoDBClient(clientConfig));
        }
    );
}

// add mediators
builder.Services.AddMediatR(typeof(CommandHandler).Assembly);
builder.Services.AddMediatR(typeof(QueryHandler).Assembly);


// GraphQL 
builder.Services.AddSingleton<ISchema, WatchlistSchema>(services => 
    new WatchlistSchema(new SelfActivatingServiceProvider(services)));

builder.Services.AddSingleton<ISchema>(s => Schema.For(@"
  type Watchlist {
    sviUserId: String!
    name: String!
  }

  type Query {
    watchlist(sviUserId: String!, name: String!): Watchlist
  }
", provider =>
{
    provider.Types.Include<Query>();}));

builder.Services.AddGraphQL(options =>
    {
        options.EnableMetrics = true;
    })
    .AddSystemTextJson()
    .AddErrorInfoProvider(opt => opt.ExposeExceptionStackTrace = true);

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
}).AddNewtonsoftJson(o => o.SerializerSettings.ReferenceLoopHandling = 
    ReferenceLoopHandling.Ignore);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Logging.AddJsonConsole();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseGraphQLAltair();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.UseGraphQL<ISchema>();

app.Run();