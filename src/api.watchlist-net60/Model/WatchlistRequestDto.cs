namespace api.watchlist_net6.Model;

public class WatchlistRequestDto
{
    public string UserId { get; set; }
    public string Name { get; set; }
    public string NewName { get; set; }
}