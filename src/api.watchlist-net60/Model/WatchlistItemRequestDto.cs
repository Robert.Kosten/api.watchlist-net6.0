namespace api.watchlist_net6.Model;

public class WatchlistItemRequestDto
{
    public string UserId { get; set; }
    public string Name { get; set; }
    public string Symbol { get; set; }
    public int? SortOrder { get; set; }
}