using System.Net;

namespace api.watchlist_net6.Model;

public class WatchlistCommandResponse
{
    public bool Success { get; set; }

    public string ErrorMessage { get; set; }

    public HttpStatusCode StatusCode { get; set; }
}