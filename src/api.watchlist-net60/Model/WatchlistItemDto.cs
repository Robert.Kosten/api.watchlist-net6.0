using Newtonsoft.Json;

namespace api.watchlist_net6.Model;

public class WatchlistItemDto
{
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string SviUserId { get; set; }
    [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
    public string Name { get; set; }
    public string Symbol { get; set; }
    public int? SortOrder { get; set; }
}