using Newtonsoft.Json;

namespace api.watchlist_net6.Model;

public class WatchlistDto
{
    [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
    public string SviUserId { get; set; }
    public string Name { get; set; }
        
    [JsonProperty("watchlistItems", NullValueHandling=NullValueHandling.Ignore)]
    public ICollection<WatchlistItemDto> WatchlistItems { get; set; }
}